import React from 'react'
import Rainbow from '../../Hoc/Rainbow'

const About = () => {
  return (
    <div className="container">
      <h4 className="center">About</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima iure saepe temporibus? Voluptatem, dolorum aspernatur.</p>
    </div>
  )
}

export default Rainbow(About)