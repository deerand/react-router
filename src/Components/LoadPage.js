import Home from './Page/Home'
import About from './Page/About'
import Contact from './Page/Contact'

export {
  Home,
  About,
  Contact
}