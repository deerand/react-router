import React, { Component } from 'react';
import Navbar from './Components/Layouts/Navbar'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Home, About, Contact } from './Components/LoadPage'
import Post from './Components/Posts/Post'



class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/about' component={About} />
            <Route path='/contact' component={Contact} />
            <Route exact path='/:post_id' component={Post} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
